jQuery.sap.require("sap.m.MessageBox");
sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/ui/model/json/JSONModel"
], function(Controller, History, JSONModel) {
	"use strict";

	return Controller.extend("sparservicesSAPServices.controller.View1", {

		// onInit: function() {
		// 		var oModel = new sap.ui.model.odata.ODataModel("model.servicesList", true);
		// 		this.getView().byId("oSel").setModel(oModel);
		// 	},
			// onInit: function() {
			// 	// 	var oModel = new JSONModel(jQuery.sap.getModulePath("SAPServices.model", "/serviceList.json"));
			// 	// 	this.getView().setModel(oModel);
			// 	// var oTable = this.getView().byId("Randomid");
			// 	// var oDataModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/sap/ZBP_MDM_TBL_SRV/", true);
			// 	var oTable = this.getView().byId("Randomid");
			// 	var bpModel = this.getOwnerComponent().getModel("MDMServiceModel");
			// 	oTable.setModel(bpModel);

		// 	oTable.addColumn(new sap.ui.table.Column({
		//          label: new sap.ui.commons.Label({
		//              text: "Info1"
		//          }),
		//          sortProperty: "INTST",
		//          template: new sap.ui.commons.TextView().bindProperty("text", "key")
		//      }));

		// oTable.bindAggregation("rows", {
		//         path: "/modelData"
		//     });
		// oTable.setEntitySet("BANKSet");
		// oTable.setInitiallyVisibleFields("BankName,BranchKey");
		// oTable.addColumn(new sap.ui.table.Column({
		// 	label: new sap.ui.commons.Label({
		// 		text: "First Name"
		// 	}),
		// 	template: new sap.ui.commons.TextField({
		// 		value: "name"
		// 	})
		// }));
		// oTable.placeAt("content");
		// },
			onClick: function() {
			var oTable = this.getView().byId("Randomid");
			// oTable.addColumn(new sap.ui.table.Column({
			// 	label: new sap.ui.commons.Label({
			// 		text: "Name"
			// 	}),
			// 	template: new sap.ui.commons.TextView({
			// 		text: {
			// 			parts: ['name', 'lastName'],
			// 			formatter: function(n, l) {
			// 				if (n && l) {
			// 					return n + ', ' + l.toUpperCase();
			// 				}
			// 			}
			// 		}
			// 	})
			// }));
			// oTable.addColumn(new sap.ui.table.Column({
			// 	label: new sap.ui.commons.Label({
			// 		text: "Info1"
			// 	})
			// }));

			var entitySet = "";
			
			entitySet = this.getView().byId("oSel").getSelectedItem().getText();
			
			if (entitySet === "BANKSet") {
				oTable.bindItems({
					path: "MDMServiceModel>/" + entitySet,
					template: new sap.m.ColumnListItem({
						cells: [
							new sap.m.Text({
								text: "{MDMServiceModel>BankNumber}"
							}),
							new sap.m.Text({
								text: "{MDMServiceModel>BankName}"
							})
						]
					})
				});
			} else if (entitySet === "CompanySet") {
				oTable.bindItems({
					path: "MDMServiceModel>/" + entitySet,
					template: new sap.m.ColumnListItem({
						cells: [
							new sap.m.Text({
								text: "{MDMServiceModel>code}"
							}),
							new sap.m.Text({
								text: "{MDMServiceModel>description}"
							})
						]
					})
				});
			} else if (entitySet === "AccountClerkSet") {
				oTable.bindItems({
					path: "MDMServiceModel>/" + entitySet,
					template: new sap.m.ColumnListItem({
						cells: [
							new sap.m.Text({
								text: "{MDMServiceModel>Busab}"
							}),
							new sap.m.Text({
								text: "{MDMServiceModel>Sname}"
							})
						]
					})
				});
			} else if (entitySet === "StoreFormatSet") {
				oTable.bindItems({
					path: "MDMServiceModel>/" + entitySet,
					template: new sap.m.ColumnListItem({
						cells: [
							new sap.m.Text({
								text: "{MDMServiceModel>code}"
							}),
							new sap.m.Text({
								text: "{MDMServiceModel>description}"
							})
						]
					})
				});
			} else if (entitySet === "ProvinceSet") {
				oTable.bindItems({
					path: "MDMServiceModel>/" + entitySet,
					template: new sap.m.ColumnListItem({
						cells: [
							new sap.m.Text({
								text: "{MDMServiceModel>regionCode}"
							}),
							new sap.m.Text({
								text: "{MDMServiceModel>regionDescription}"
							})
						]
					})
				});
			} else if (entitySet === "AccountStatementSet") {
				oTable.bindItems({
					path: "MDMServiceModel>/" + entitySet,
					template: new sap.m.ColumnListItem({
						cells: [
							new sap.m.Text({
								text: "{MDMServiceModel>periodicAccountStatementIndicator}"
							}),
							new sap.m.Text({
								text: "{MDMServiceModel>periodicAccountStatementText}"
							})
						]
					})
				});
			} else if (entitySet === "PaymentMethodsSet") {
				oTable.bindItems({
					path: "MDMServiceModel>/" + entitySet,
					template: new sap.m.ColumnListItem({
						cells: [
							new sap.m.Text({
								text: "{MDMServiceModel>name}"
							}),
							new sap.m.Text({
								text: "{MDMServiceModel>method}"
							})
						]
					})
				});
			} else if (entitySet === "PaymentTermsSet") {
				oTable.bindItems({
					path: "MDMServiceModel>/" + entitySet,
					template: new sap.m.ColumnListItem({
						cells: [
							new sap.m.Text({
								text: "{MDMServiceModel>code}"
							}),
							new sap.m.Text({
								text: "{MDMServiceModel>description}"
							})
						]
					})
				});
			} else if (entitySet === "PurchaseOrganisationSet") {
				oTable.bindItems({
					path: "MDMServiceModel>/" + entitySet,
					template: new sap.m.ColumnListItem({
						cells: [
							new sap.m.Text({
								text: "{MDMServiceModel>code}"
							}),
							new sap.m.Text({
								text: "{MDMServiceModel>description}"
							})
						]
					})
				});
			} else if (entitySet === "PurchOrgCompanySet") {
				oTable.bindItems({
					path: "MDMServiceModel>/" + entitySet,
					template: new sap.m.ColumnListItem({
						cells: [
							new sap.m.Text({
								text: "{MDMServiceModel>purchOrgCode}"
							}),
							new sap.m.Text({
								text: "{MDMServiceModel>purchOrgDescription}"
							})
						]
					})
				});
			} else if (entitySet === "CompanyTypeSet") {
				oTable.bindItems({
					path: "MDMServiceModel>/" + entitySet,
					template: new sap.m.ColumnListItem({
						cells: [
							new sap.m.Text({
								text: "{MDMServiceModel>companyCode}"
							}),
							new sap.m.Text({
								text: "{MDMServiceModel>paramType}"
							})
						]
					})
				});
			}

		}

		// onClick: function(){
		// 	// var countryCode = this.getView().byId("countrycode").getValue();
		// 	// var companyCode = this.getView().byId("companycode").getValue();

		// 	var oForm = this.getView().byId("idSimpleForm");
		// 	// oForm.bindElement("zbpExtendMode>/A_BusinessPartner('401137')");
		// 	oForm.bindElement("MDMServiceModel>/BankNameSet");

		// }
	});
});